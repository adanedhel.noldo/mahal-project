// содежимое index.js
const http = require('http')
const port = 3000
const requestHandler = (request, response) => {
    console.log(request.url)
    let r = Math.round(Math.random() * 100)
    response.end('Your random for this time is: ' + r)
}
const server = http.createServer(requestHandler)

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})